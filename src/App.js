import React, {Component}from "react";

import "./App.css";
import Photo from "./components/Photo";
import Body from "./components/Body";
import styled from "styled-components";


const Top = styled.button
`
position:fixed;
top:91%;
right:5%;
background-color:transparent;
border:0px solid black;

z-index:100;
height:auto;
width:auto;

`

const scrollTop = () =>{
  window.scrollTo({top: 420, behavior: 'smooth'});
  window.history.pushState('', '', '/');

};
export default function App() {

return (
    <>
      <Photo id="photos" />
      <Body />
    
    <Top onClick={scrollTop} id="top">
    <i class="fas fa-arrow-circle-up fa-3x"></i>


    </Top>
    </>
  );
}


