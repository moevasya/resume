import React from "react";
import styled from "styled-components";
import CP from "../img/CP.jpg";
import WBP from "../img/WBP.png";

const ProjDiv = styled.div`
  display: flex;
  flex-direction: column;
  width: auto;
  height: auto;
  padding: 5% 15%;
  margin: 0px;
  background-color: ;
  text-align: center;
  color: black;
  @media only screen and (max-height: 1025px) {
    @media only screen and (max-width: 769px) {
      padding: 5%;
    }
  }
  @media only screen and (max-width: 500px) {
    padding: 5%;
  }
`;
const ProjImg = styled.img`
  height: 300px;
  width: 500px;
  margin-top: 10px;
  margin-right: auto;
  margin-left: auto;
  border-radius: 20px;

  @media only screen and (max-width: 500px) {
    width: 320px;
    height: 200px;
  }
  @media only screen and (max-width: 400px) {
    width: 260px;
    padding: 10px;
  }
`;
const Subcont = styled.div`
  display: flex;
  flex-direction: row;
`;
export default function Projects() {
  return (
    <ProjDiv id="projects" className="panel">
      <h1>Projects</h1>

      <h2>Simple Calculator</h2>
      <ProjImg src={CP} />
      <p className="description">
        I designed and developed this simple calculator using ReactJs and other
        varieties of JavaScript Libraries such as styled-components. <br></br>I
        take pride in writing the whole algorithm and debugging it on my own
        without the help of any online resources, I considered it as a challenge
        for me and my algorithm writing skills and it wasn't an easy task to do.
        <br></br>
        On the other hand I have used online resources for some components such
        as the animated background.
      </p>
      <a
        href="https://gitlab.com/hellHole/calculator"
        id="pl"
        className="Links"
        target="_blank"
      >
        {" "}
        <h2>GitLab Repo</h2>
      </a>
      <hr class="solid"></hr>

      <h2>Local Weather</h2>
      <ProjImg src={WBP} />
      <p className="description">
        This app was also designed and developed using ReactJs and a variety of
        JS libraries such as Styled-Components, axios, React-Router-Dom and
        ReactJS Bootstrap.<br></br>I have also used OpenWeather API to provide
        Real-time Data about the weather condition in different areas and cities
        across Jordan.<br></br>
      </p>
      <a
      href="https://weather-bonanza-c8121.firebaseapp.com/"
      id="pl"
      className="Links "
      target="_blank"
      >
        {" "}
        <h3>Weather Bonanza Main Page</h3>

      </a>
      <a
        href="https://gitlab.com/moevasya/weatherbonanza"
        id="pl"
        className="Links inline"
        target="_blank"
      >
        {" "}
        <h2>GitLab Repo</h2>
      </a>
    </ProjDiv>
  );
}
