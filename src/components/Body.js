import React from "react";
import styled from "styled-components";
import ProgressBar from "react-customizable-progressbar";
import Skillsf from "./Skills";
import AboutMef from "./AboutMe";
import Projects from "./Projects";
import Experience from "./Experience";
export default function Body() {
  return (
    <>
      <AboutMef />
      <hr class="solid"></hr>

      <Skillsf />
      <hr class="solid"></hr>

      <Projects />
      <hr class="solid"></hr>

      <Experience />
      <hr class="solid"></hr>
    </>
  );
}
