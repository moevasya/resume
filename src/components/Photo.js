import React from "react";
import styled from "styled-components";
import Pic from "../img/Pic.jpg";
import {
  Facebook,
  Gitlab,
  Linkedin,
  Twitter,
  Instagram,
  Mail,
  Phone,
} from "react-feather";

const Container = styled.div`
  display: flex;
  background-color: #add8e6;
  top: 0;
  width: 100%;
  height: 650px;
  flex-direction: column;
  margin-left: auto;
  margi-right: auto;
  @media only screen and (max-height: 1024px) {
    @media only screen and (max-width: 768px) {
    }
  }
`;

const Image = styled.img`
  border-radius: 50%;
  display: flex;

  height: 250px;
  width: 250px;
  margin-top: 100px;
  margin-right: auto;
  margin-left: auto;
`;
const Info = styled.section`
  display: flex;
  margin-top: 20px;
  color: white;
  font-family: sans-serif;
  font-size: 30px;
  margin-right: auto;
  margin-left: auto;
  text-align: center;
`;
const Social = styled.section`
  margin-top: 20px;
  margin-right: auto;
  margin-left: auto;
`;
function phone() {
  var x = document.getElementById("phone");
  var y = document.getElementById("email");

  if (x.style.display === "none") {
    x.style.display = "block";
    y.style.display = "none";
  } else {
    x.style.display = "none";
    y.style.display = "none";
  }
}
function email() {
  var x = document.getElementById("phone");

  var y = document.getElementById("email");
  if (y.style.display === "none") {
    x.style.display = "none";

    y.style.display = "block";
  } else {
    y.style.display = "none";
    x.style.display = "none";
  }
}
function phonecopy() {
  var copyText = document.getElementById("phone");

  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  document.execCommand("copy");

  alert("Copied the text: " + copyText.value);
}
function emailcopy() {
  var copyText = document.getElementById("email");

  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  document.execCommand("copy");

  alert("Copied the text: " + copyText.value);
}
export default function Photo() {
  return (
    <Container id="photo">
      <Image src={Pic} />
      <Info>
        Mohammad Atharbeh <br></br>
        20-3-1997 <br></br>
        Al-Balqa'a Applied University
        <br></br> Faculty of IT - Computer Science
      </Info>
      <Social>
        <a href="https://www.facebook.com/VasielyGA.97/" target="_blank">
          {" "}
          <Facebook className="Icons" />
        </a>
        <a href="https://gitlab.com/moevasya" target="_blank">
          <Gitlab className="Icons" />
        </a>
        <a
          href="https://www.linkedin.com/in/mohammad-atharbeh-8a6227185/"
          target="_blank"
        >
          <Linkedin className="Icons" />
        </a>
        <a href="https://twitter.com/mohammadadarbe2" target="_blank">
          <Twitter className="Icons" />
        </a>
        <a onClick={email}>
          <Mail className="Icons" />
        </a>

        <a onClick={phone}>
          <Phone className="Icons" />
        </a>
        <div id="email" className="popup">
          mohammad.atharbeh.97@gmail.com
        </div>
        <div id="phone" className="popup">
          +962780399611
        </div>
      </Social>
    </Container>
  );
}
