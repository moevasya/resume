import React from "react";
import styled from "styled-components";
import ProgressBar from "react-customizable-progressbar";

const Exp = styled.div`
  display: flex;
  flex-direction: column;
  width: auto;
  height: auto;
  background-color: #a95aec;
  marging: 0px;
  padding: 5% 25%;
  text-align: center;
  color:#fff;

  @media only screen and (max-height: 1025px) {
    @media only screen and (max-width: 769px) {
      padding: 5px;
    }
  }
`;

const OC = styled.div`
  display: flex;
  flex-direction: column;
  width: auto;
  height: auto;
  background-color: ;
  marging: 0px;
  padding: 5% 25%;
  text-align: center;
  @media only screen and (max-height: 1025px) {
    @media only screen and (max-width: 769px) {
      padding: 5%;
    }
  }
  @media only screen and (max-width: 400px) {
    padding: 15px;
  }
`;
const Row = styled.section`
  display: flex;
  flex-direction: row;
  margin-right: auto;
  margin-left: auto;
  backgroun-color: transparent;

  @media only screen and (max-height: 1025px) {
    @media only screen and (max-width: 769px) {
      flex-direction: column;
    }
  }
  @media only screen and (max-width: 320px) {
  }
`;
const SRow = styled.section`
  display: flex;
  flex-direction: row;
  margin-right: auto;
  margin-left: auto;
  backgroun-color: transparent;
  @media only screen and (max-width: 400px) {
    margin-left: -10px;
  }
`;
const Col = styled.section`
  display: flex;
  flex-direction: Column;
  margin-right: 20px;
  margin-left: auto;
  backgroun-color: transparent;

  margin-left: 5px;
  padding-right: 5px;
  @media only screen and (max-width: 400px) {
    padding: 0px;
    margin-right: auto;
  }
`;
const SCol = styled.section`
  display: flex;
  flex-direction: Column;
  margin-right: 20px;
  margin-left: auto;
  backgroun-color: transparent;

  margin-left: 5px;
  padding-right: 5px;
  @media only screen and (max-width: 400px) {
    padding: 0px;
    margin-right: auto;
    margin-left: -8px;
  }
`;
const SL = styled.div`
  display: flex;
  flex-direction: column;
  width: auto;
  height: auto;
  background-color: #98f1a8;
  marging: 0px;
  padding: 5% 10%;
  text-align: center;
  color: white;
  @media only screen and (max-height: 1025px) {
    @media only screen and (max-width: 769px) {
      flex-direction: column;
    }
  }
  @media only screen and (max-width: 400px) {
    padding: 5%;
  }
`;
export default function Experience() {
  return (
    <>
      <Exp id="exp">
        <h1>
          Experience<br></br>
        </h1>

        <h2>Run Jordan - Timing System Official/Part Time</h2>
        <h3>2018-Present</h3>
        <p className="description">
          when i was a softmore student in college i was nominated by a friend
          to work with Run Jordan as a Timing System Official, I am responsible
          for operating and maintaining the Timing Device during different
          marathons across jordan.
        </p>
      </Exp>
      <hr class="solid"></hr>

      <OC id="oc">
        <h1>Online Courses</h1>

        <h2>Youtube courses</h2>

        <SRow>
          <Col>
            <h4> HTML5 - Elzero Web School </h4>

            <h4> CSS3 - Elzero Web School</h4>

            <h4>Bootstrap - Elzero Web School</h4>

            <h4> JavaScript - Elzero Web School</h4>
          </Col>
          <Col>
            <h4> ReactJS - Quinten Watt</h4>

            <h4>TypeScript - Traversy Media</h4>

            <h4> TypeScript - Traversy Media</h4>

            <h4> Styled-Components - Fidalgo</h4>
          </Col>
        </SRow>
        <h4> Java OOP 101 + 102 + 103 - Abdullah Eid </h4>
        <hr class="solid"></hr>

        <h2>Udemy courses</h2>
        <h4> The Complete Android N Developer - Rob Percival</h4>
      </OC>
      <hr class="solid"></hr>

      <SL id="sl">
        <h1> Languages </h1>
        <h2>Arabic - Mother Tongue</h2>
        <hr class="solid"></hr>

        <h2>English</h2>
        <Row className="languages">
          <ProgressBar
            className="bars"
            radius={70}
            progress={90}
            strokeWidth={4}
            strokeColor="#5382a1"
            trackStrokeWidth={4}
            pointerRadius={8}
            pointerStrokeWidth={5}
            cut={120}
            rotate={-210}
            pointerStrokeColor="#5382a1"
            initialAnimation={true}
            transition="1.5s ease 0.5s"
            trackTransition="0s ease"
          >
            <p className="Javap" style={{ color: "#5382a1" }}>
              90%<br></br>Reading
            </p>
          </ProgressBar>
          <ProgressBar
            className="bars"
            radius={70}
            progress={90}
            strokeWidth={4}
            strokeColor="blue"
            trackStrokeWidth={4}
            pointerRadius={8}
            pointerStrokeWidth={5}
            cut={120}
            rotate={-210}
            pointerStrokeColor="blue"
            initialAnimation={true}
            transition="1.5s ease 0.5s"
            trackTransition="0s ease"
          >
            <p className="Javap" style={{ color: "blue" }}>
              90%<br></br>Writing
            </p>
          </ProgressBar>
          <ProgressBar
            className="bars"
            radius={70}
            progress={90}
            strokeWidth={4}
            strokeColor="red"
            trackStrokeWidth={4}
            pointerRadius={8}
            pointerStrokeWidth={5}
            cut={120}
            rotate={-210}
            pointerStrokeColor="red"
            initialAnimation={true}
            transition="1.5s ease 0.5s"
            trackTransition="0s ease"
          >
            <p className="Javap" style={{ color: "red" }}>
              90%<br></br>Speaking
            </p>
          </ProgressBar>
        </Row>
        <hr class="solid"></hr>

        <h2>Russian</h2>
        <Row>
          <ProgressBar
            className="bars"
            radius={70}
            progress={50}
            strokeWidth={4}
            strokeColor="#5382a1"
            trackStrokeWidth={4}
            pointerRadius={8}
            pointerStrokeWidth={5}
            cut={120}
            rotate={-210}
            pointerStrokeColor="#5382a1"
            initialAnimation={true}
            transition="1.5s ease 0.5s"
            trackTransition="0s ease"
          >
            <p className="Javap" style={{ color: "#5382a1" }}>
              50%<br></br>Reading
            </p>
          </ProgressBar>
          <ProgressBar
            className="bars"
            radius={70}
            progress={30}
            strokeWidth={4}
            strokeColor="blue"
            trackStrokeWidth={4}
            pointerRadius={8}
            pointerStrokeWidth={5}
            cut={120}
            rotate={-210}
            pointerStrokeColor="blue"
            initialAnimation={true}
            transition="1.5s ease 0.5s"
            trackTransition="0s ease"
          >
            <p className="Javap" style={{ color: "blue" }}>
              30%<br></br>Writing
            </p>
          </ProgressBar>
          <ProgressBar
            className="bars"
            radius={70}
            progress={50}
            strokeWidth={4}
            strokeColor="red"
            trackStrokeWidth={4}
            pointerRadius={8}
            pointerStrokeWidth={5}
            cut={120}
            rotate={-210}
            pointerStrokeColor="red"
            initialAnimation={true}
            transition="1.5s ease 0.5s"
            trackTransition="0s ease"
          >
            <p className="Javap" style={{ color: "red" }}>
              50%<br></br>Speaking
            </p>
          </ProgressBar>
        </Row>
        <hr class="solid"></hr>

        <h1>Skills</h1>
        <SRow>
          <SCol>
            <h3>Fast Learner</h3>
            <h3>Excellent Communication</h3>
            <h3>Ability to Work Under Pressure</h3>
          </SCol>

          <SCol>
            <h3>Teamworker </h3>

            <h3>Punctual </h3>
            <h3>Drivers License </h3>
          </SCol>
        </SRow>
      </SL>
    </>
  );
}
