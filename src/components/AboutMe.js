import React from "react";
import styled from "styled-components";

const NavCont = styled.section`
  display: flex;
  flex-direction: row;
  font-family: sans-serif;
  padding-left: 10%;
  padding-right: 10%;
  margin-top: 5px;
  font-weight: bold;
  @media only screen and (max-height: 1024px) {
    @media only screen and (max-width: 768px) {
      padding-left: 2%;
      padding-right: 2%;
      margin-top: 5px;
    }
  }
  @media only screen and (max-width: 400px) {
    padding: 0%;
  }
`;

const AboutMe = styled.div`
  display: flex;
  flex-direction: column;
  height: auto;
  background-color: white;
  padding: 100px 300px;
  text-align: center;
  color: black;
  font-size: 25px;
  @media only screen and (max-height: 1024px) {
    @media only screen and (max-width: 768px) {
      padding: 50px;
    }
  }

  @media only screen and (max-width: 500px) {
  }
`;

export default function AboutMef() {
  return (
    <>
      <NavCont>
        <a className="Links" href="#photo">
          Mohammad Atharbeh
        </a>

        <a className="Links" href="#aboutme">
          About Me
        </a>
        <a className="Links" href="#tskills">
          Technical Skills
        </a>
        <a className="Links" href="#projects">
          Projects
        </a>
        <a className="Links" href="#exp">
          Experience
        </a>
        <a className="Links" href="#oc">
          Online Courses
        </a>
        <a className="Links" href="#sl">
          Skills and Languagges
        </a>
      </NavCont>
      <hr class="solid"></hr>

      <AboutMe id="aboutme">
        <h1>About Me</h1>
        <p>
          A Motivated Student with the GPA of 2.85 as a CS major in al-balqa'a
          applied university, always looking for something new to learn in the
          field of Web Design and Android Application Development . I have
          achieved good grades at the courses of my study, along with working on
          my skills by taking various online courses in Android Development and
          Web Design. I'm a team worker, I can handle pressure well, punctual,
          and always on the look of new knowledge within my field of expertise.
        </p>
      </AboutMe>
    </>
  );
}
